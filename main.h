// Nathaniel Roach - 2017
// GNU GPLv2

#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <inttypes.h>
#include "mqtt.h"
#include "wol.h"
#include "types.h"

#define PAYLOAD_PADDING 0
#define PAYLOAD_MC_COUNT 16
#define MQTT_MESSAGE_LEN 6


#endif
