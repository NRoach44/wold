// Nathaniel Roach - 2017
// GNU GPLv2

#ifndef _TYPES_H_
#define _TYPES_H_

typedef struct {
  const char *MQTTHostname;
  int MQTTPort;
  const char *MQTTTopic;
  bool newMsg;
  char inMAC[65];
  uint8_t currentMAC[6];
  bool haveIP4;
  unsigned int currentIP4[4];
  struct mosquitto *mosqInstance;
} WOLData;

#endif
