// Wake On Lan Daemon
// waits for a certain MQTT message and then wakes the computer
// Nathaniel Roach - 2017, 2020
// GNU GPLv2

#include "main.h"

int runLoop = 1;

void handleSig(int sig) {
  printf("Recieved signal: %i\r\n", sig);
  runLoop = 0;
}

int parseMACIn(WOLData *instanceData) {

  uint8_t *mac = (*instanceData).currentMAC;
  unsigned int *ipv4 = (*instanceData).currentIP4;
  int count;

  char *suffix = (char*)calloc(32,sizeof(char));

  (*instanceData).haveIP4 = false;

  if (strlen((*instanceData).inMAC) >= 17 && strlen((*instanceData).inMAC) <= 33) { // 16 characters for MAC, and 7+ for IPv4
    // We're expecting 00:00:00:00:00:00, or 00:00:00:00:00:00-255.255.255.255
#ifdef __DEBUG
    printf("main: parsing '%s'\r\n", (*instanceData).inMAC);
#endif // __DEBUG

    count = sscanf((*instanceData).inMAC, "%2hhx:%2hhx:%2hhx:%2hhx:%2hhx:%2hhx%15s",
                   mac, mac+1, mac+2, mac+3, mac+4, mac+5,
                   suffix);

#ifdef __DEBUG
    printf("main: successfully parsed %i elements\r\n", count);
#endif
    if (count == MQTT_MESSAGE_LEN || count == MQTT_MESSAGE_LEN + 1) {
#ifdef __DEBUG
      printf("main: found MAC of %hhx:%hhx:%hhx:%hhx:%hhx:%hhx\r\n",
             mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
#endif // __DEBUG

      if (count == 7) {
        int countIP;

        countIP = sscanf(suffix, "-%u.%u.%u.%u",
                         ipv4, ipv4+1, ipv4+2, ipv4+3);

        if (countIP == 4) {
          (*instanceData).haveIP4 = true;
#ifdef __DEBUG
          printf("main: found IP of %u.%u.%u.%u\r\n",
                 ipv4[0], ipv4[1], ipv4[2], ipv4[3]);
#endif // __DEBUG
        } else {
          printf("main: message appears to have a valid MAC but invalid IP, ignoring (%s)\r\n", (*instanceData).inMAC);
        }

      }
    } else {
      printf("main: could not parse MQTT message (%i)\r\n", count);
    }

  } else {
    //#ifdef __DEBUG
    printf("main: message incorrect size (%li)\r\n", strlen((*instanceData).inMAC));
    //#endif // __DEBUG
    count = 0;
  }

  free(suffix);
  return count;
}

int sendPacket(WOLData *instanceData, uint8_t *payload, int payloadLen) {
  WOLDest destination;

  destination.haveIP4 = false;

  for (int i = 0; i < 6; i++) {
    destination.machine[i] = (*instanceData).currentMAC[i];
    //printf("%02X:", destination.machine[i]);
  }
  if ((*instanceData).haveIP4) {
    destination.haveIP4 = true;
    for (int i = 0; i < 4; i++)
      destination.ip4Addr[i] = (uint8_t)(*instanceData).currentIP4[i];
  }
  wolSend(destination, payload, payloadLen);
  /*
  for (int i = 0; i < payloadLen; i++){
    printf("%02X:", payload[i]);
  } */
  //runLoop = 0;
  return 0;
}

int main(int argc, char *argv[]) {
  // Allocate enough memory for the payload - 6 x 0xFF + 16 x MAC + padding
  int payloadLen = (6 + PAYLOAD_MC_COUNT*6 + PAYLOAD_PADDING);
  uint8_t *payload = (uint8_t*)calloc(payloadLen,sizeof(uint8_t));
  WOLData *instanceData = (WOLData*)calloc(1,sizeof(WOLData));
  int opt, portIn;
  int retval = 0;

  signal(SIGINT, handleSig);
  signal(SIGTERM, handleSig);

  printf("MQTT Wake On Lan Daemon - (C) NRoach44\r\n");
  printf("Built %s %s\r\n", __DATE__, __TIME__);
#ifdef __DEBUG
  printf("Debug build\r\n");
#endif // __DEBUG

  while ((opt = getopt(argc, argv, "h:p:")) != -1) {
    switch (opt) {

    case 'h': // MQTT Hostname to connect to
      (*instanceData).MQTTHostname = optarg;
#ifdef __DEBUG
      printf("main: connecting to host %s\r\n", (*instanceData).MQTTHostname);
#endif // __DEBUG
      break;

    case 't': // MQTT topic to listen to
      (*instanceData).MQTTTopic = optarg;
#ifdef __DEBUG
      printf("main: subscribing to topic %s\r\n", (*instanceData).MQTTTopic);
#endif // __DEBUG
      break;


    case 'p': // MQTT server port name
      sscanf(optarg, "%i", &portIn);
      if (portIn) {
        (*instanceData).MQTTPort = portIn;
#ifdef __DEBUG
        printf("main: connecting to port %i\r\n", (*instanceData).MQTTPort);
#endif // __DEBUG
      }
      break;
    }
  }

  // If we have one of host and port, quit
  if (((*instanceData).MQTTHostname == NULL) || ((*instanceData).MQTTPort == 0)) {
    printf("Usage: wold -h <server hostname> -p <server port> -t [topic] \r\n");
    runLoop = 0;
  }

  // If we have neither, assume localhost
  if (((*instanceData).MQTTHostname == NULL) && ((*instanceData).MQTTPort == 0)) {
    (*instanceData).MQTTHostname = "localhost";
    (*instanceData).MQTTPort = 1883;
    printf("Assuming %s:%i\r\n", (*instanceData).MQTTHostname, (*instanceData).MQTTPort);
    runLoop = 1;
  }

  // Assume default topic if not specified
  if ((*instanceData).MQTTTopic == NULL) {
    (*instanceData).MQTTTopic = MQTT_TOPIC;
    printf("main: Subscribing to %s\r\n", (*instanceData).MQTTTopic);
  }

  // Setup the MQTT side of things
  if (runLoop) {
    mqttSetup(instanceData);
    if ((*instanceData).mosqInstance == NULL) {
      runLoop = 0;
    }
  }

  // Main loop
  while (runLoop) {

    if (!mqttLoop(instanceData)) {
      fflush(stdout);

      if ((*instanceData).newMsg) { // If we have a new message, handle it
        (*instanceData).newMsg = 0; // Clear new message flag

        if (parseMACIn(instanceData) >= MQTT_MESSAGE_LEN) { //If we have a valid payload
          printf("main: got a new message...\r\n");
          sendPacket(instanceData, payload, payloadLen);
          //runLoop = 0; // Uncomment to have some way of closing the process when running valgrind
        }

      }

    } else {
      runLoop = 0;
      retval = 1;
    }

  }
  mqttEnd(instanceData);
  free(payload);
  free(instanceData);

  return retval;
}
