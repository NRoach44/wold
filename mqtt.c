// Nathaniel Roach - 2017
// GNU GPLv2

#include "mqtt.h"

void handleConnect(struct mosquitto *mosq, void *obj, int result) {
  //printf("mqtt: connected (%i)\n", result);
  if (result) {
    printf("mqtt: connect returned %i : %s\r\n", result, strerror(result));
  } else {
    printf("mqtt: Connected\r\n");
  }
}

void handleMsg(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message) {

  WOLData *instanceData =  (WOLData*) obj;

#ifdef __DEBUG
  printf("mqtt: recieved '%s'(%i):'%.*s'\r\n", (*message).topic, (*message).payloadlen, (*message).payloadlen, (char*)(*message).payload);
#endif // __DEBUG

  mosquitto_topic_matches_sub(MQTT_TOPIC, (*message).topic, &(*instanceData).newMsg);

  if ((*instanceData).newMsg) {
#ifdef __DEBUG
    printf("mqtt: message matches topic\r\n");
#endif
    if ((*message).payloadlen > 63) {
      memcpy((*instanceData).inMAC, (*message).payload, 63);
      (*instanceData).inMAC[64] = 0x00;
    } else {
      memcpy((*instanceData).inMAC, (*message).payload, (*message).payloadlen);
      (*instanceData).inMAC[(*message).payloadlen] = 0x00;
    }
  }
}

void mqttSetup(WOLData *instanceData) {
  struct mosquitto *mosq;

  mosquitto_lib_init();
  mosq = mosquitto_new(NULL, 1, (void*)instanceData); // Random ID
  if (mosq == NULL) {
    printf("mqtt: Error initialising: %i (%s)", errno, strerror(errno));
    return;
  }
  (*instanceData).mosqInstance = mosq;

  mosquitto_connect_callback_set(mosq, handleConnect);
  mosquitto_message_callback_set(mosq, handleMsg);

  mosquitto_connect(mosq, (*instanceData).MQTTHostname, (*instanceData).MQTTPort, 60);
  mosquitto_subscribe(mosq, NULL, MQTT_TOPIC, 0);
}

int mqttLoop(WOLData *instanceData) {
  int retval;

  retval = mosquitto_loop((*instanceData).mosqInstance, -1, 1);
  if (retval) {
    printf("mqtt: error polling server: %i\r\n", retval);
  } else {
    sleep(1);
  }
  return retval;
}

void mqttEnd(WOLData *instanceData) {
  mosquitto_destroy((*instanceData).mosqInstance);
}
