// Nathaniel Roach - 2017
// GNU GPLv2

#ifndef _WOL_H_
#define _WOL_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

typedef struct {
  //struct sockaddr_in network;
  uint8_t machine[6];
  bool haveIP4;
  uint8_t ip4Addr[4];
} WOLDest;

int wolSend(WOLDest destination, uint8_t *payload, int payloadLen);
#endif
