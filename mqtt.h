// Nathaniel Roach - 2017
// GNU GPLv2

#ifndef _MQTT_H_
#define _MQTT_H_

#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <errno.h>

#include <mosquitto.h>
#include "types.h"

#define MQTT_HOST "localhost"
#define MQTT_PORT 1883

#define MQTT_TOPIC "/com/wold/target"

void mqttSetup(WOLData *instanceData);
int mqttLoop(WOLData *instanceData);
void handleConnect(struct mosquitto *mosq, void *obj, int result);
void handleMsg(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);
void mqttEnd(WOLData *instanceData);
#endif
