// Nathaniel Roach - 2017
// GNU GPLv2

#include "wol.h"

int wolSend(WOLDest destination, uint8_t *payload, int payloadLen) {
  int location;
  struct ifaddrs *ifaddr, *tmp;
  struct in_addr targetAddr, brdcstAddr;
  uint32_t ifaddress, ifmask, ifbrdcst, destip4;
  bool foundInterface = 0;

  char *targetAddrS = (char*)calloc(16,sizeof(char));

  const char* hostname_default="10.1.10.255";
  const char *hostname;
  const char* portname="6969";
  struct addrinfo hints;

  hostname = hostname_default;
  // build packet

  // build initial 0xFF header
  for (int i = 0; i < 6; i++) {
    payload[i] = 0xFF;
  };

  // build MAC address section - 16 sets of the MAC
  for (int j = 0; j < 16; j++) {
    for (int h = 0; h < 6; h++) {
      location = 6 + (j*6) + h;
      payload[location] = destination.machine[h];
    }
  }
  if (destination.haveIP4) {
    // Determine what IP Address to send the datagram to

    sprintf(targetAddrS, "%u.%u.%u.%u",
            destination.ip4Addr[0],
            destination.ip4Addr[1],
            destination.ip4Addr[2],
            destination.ip4Addr[3]);
#ifdef __DEBUG
    printf("wol: input IP: %s\r\n", targetAddrS);
#endif // __DEBUG
    if (inet_aton(targetAddrS, &targetAddr) == 0) {
      printf("wol: error parsing IP address, ignoring\r\n");
    } else {
#ifdef __DEBUG
      printf("wol: parsed target addr %s\r\n", inet_ntoa(targetAddr));
#endif // __DEBUG

      if (getifaddrs(&ifaddr) == -1) {
        printf("wol: unknown error getting interface info, sending to 10.1.10.255 by default\r\n");
      } else {
        tmp = ifaddr;

#ifdef __DEBUG
        printf("wol: iterating over active network interfaces to find a broadcast domain\r\n");
#endif // __DEBUG

        destip4 = ((targetAddr).s_addr);

        while (tmp && !(foundInterface)) {
          if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_INET) {
            struct sockaddr_in *pAddr = (struct sockaddr_in *)tmp->ifa_addr;
            struct sockaddr_in *pMask = (struct sockaddr_in *)tmp->ifa_netmask;

            ifaddress = ((*pAddr).sin_addr).s_addr;
            ifmask = ((*pMask).sin_addr).s_addr;

#ifdef __DEBUG
            printf("wol: %s: A %s ", tmp->ifa_name, inet_ntoa(pAddr->sin_addr));
            printf("B %s \r\n", inet_ntoa(pMask->sin_addr));
#endif // __DEBUG

            if ((ifaddress & ifmask) == (destip4 & ifmask)) {
#ifdef __DEBUG
              printf("wol: found interface with matching broadcast domain (%s)\r\n", tmp->ifa_name);
#endif // __DEBUG
              foundInterface = 1;
            }
          }
          tmp = tmp->ifa_next;
        }
        if (foundInterface) {
          ifbrdcst = ((0xFFFFFFFF ^ ifmask) + (ifaddress & ifmask));
          brdcstAddr.s_addr = ifbrdcst;
          printf("wol: IP address is on a local network, sending broadcast packet to %s\r\n", inet_ntoa(brdcstAddr));
          hostname = inet_ntoa(brdcstAddr);
        } else {
          printf("wol: couldn't find interface with matching broadcast domain, sending unicast packet instead \r\n");
          hostname = inet_ntoa(targetAddr);
        }
        freeifaddrs(ifaddr);
      }
    }
  }

  // sendto
  // http://www.microhowto.info/howto/send_a_udp_datagram_in_c.html

  printf("wol: sending message to %s", hostname);

#ifdef __DEBUG
  printf(":%s with length %i", portname, payloadLen);
#endif // __DEBUG

  printf("\r\n");

  memset(&hints,0,sizeof(hints));
  hints.ai_family=AF_UNSPEC;
  hints.ai_socktype=SOCK_DGRAM;
  hints.ai_protocol=0;
  hints.ai_flags=AI_ADDRCONFIG;
  struct addrinfo* res=0;
  int err=getaddrinfo(hostname,portname,&hints,&res);
  if (err!=0) {
    printf("failed to resolve remote socket address (err=%d)",err);
  }
  int fd=socket(res->ai_family,res->ai_socktype,res->ai_protocol);
  if (fd==-1) {
    printf("%s",strerror(errno));
  }

  int broadcast=1;
  setsockopt(fd,SOL_SOCKET,SO_BROADCAST,&broadcast,sizeof(broadcast));

  if (sendto(fd,payload,102,0,res->ai_addr,res->ai_addrlen)) {
#ifdef __DEBUG
    printf("wol: sendto reports %s\r\n", strerror(errno));
#endif // __DEBUG
  }

  free(targetAddrS);
  freeaddrinfo(res);
  return 0;
}
